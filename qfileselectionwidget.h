#ifndef QFILESELECTIONWIDGET_H
#define QFILESELECTIONWIDGET_H

#include <QtWidgets/QWidget>
#include <QFileDialog>
#include <QDir>
#include <QDragEnterEvent>
#include <QMimeData>
#include <QDebug>

namespace Ui {
class QFileSelectionWidget;
}

/*!
 *  \ingroup QWidgets
 *  \class QFileSelectionWidget
 *  \brief \c QWidget used to browse for and select a file.
 *
 *  \c QWidget used to browse for and select a file.\n
 *  This \c QWidget is a combination of a \c QLineEdit and a \c QPushButton.\n
 *  When the user presses the button, a file selection window will appear and, after the user selects a file, the full path (or only the file name, depends on properties) will be shown in the \c QLineEdit.\n
 *  This class \ccb_{DOES NOT} depend on \c "norlib.h".
 *  \par Properties
 *  - \ccb_{show_path_mode}: \format_param_type{bool} : controls whether the full path of the file is shown in the \c QLineEdit, (\c true), or only the file's name (\c false).
 *      When the \c show_path_mode is changed, the \c QLineEdit refreshes its contents immediately.
 *      It defaults to \ccb_{true}.
 *  - \ccb_{editable_path_state}: \format_param_type{bool} : controls whether the \c QLineEdit containing the path is editable (\c true) or not (\c false).
 *      It defaults to \ccb_{false}.
 *  - \ccb_{file_open_save_mode}: \format_param_type{bool} : controls whether the \c QFileDialog spawned to browse for a file will be in mode 'open' (\c true) or 'save' (\c false).
 *      It defaults to \ccb_{true}.
 *  - \ccb_{remember_last_path}: \format_param_type{bool} : controls whether browse file dialog will open by default on the last used path (\c true), or in \c basepath (\c false).
 *      It defaults to \ccb_{true}.
 *  - \ccb_{basepath}: \format_param_type{QString} : the base path of the QDialog. If it's empty, the current working directory will be used.
 *  - \ccb_{browse_extension_filter}: \format_param_type{QString} : extension filter for the dialog (Qt File Filter syntax). If it's empty, all files will be shown.
 *  - \ccb_{accepts_drops}: \format_param_type{bool} : indicates if the \c QFileSelectionWidget accepts drag-and-drop requests from files (extensions accepted depend on \c browse_extension_filter).
 *  \par Example of use
 *  \code
 *      // Example use of QFileSelectionWidget
 *
 *          QFileSelectionWidget *qfsw = new QFileSelectionWidget(ui->widget_example);  // We want to replace the QWidget pointed by "ui->widget_example".
 *          qfsw->conf_flip_show_path_mode();                                           // We can change the properties before the substitution of the UI pointer in the next line
 *          ui->widget_example = qfsw;                                                  // Updating the UI pointer
 *          qfsw->conf_set_show_path_mode(true);                                        // Properties can be changed after the substitution too!
 *  \endcode
 */
class QFileSelectionWidget : public QWidget
{
    Q_OBJECT

public:
    /*!
     * \brief Class constructor
     * \param widget_to_replace : \format_param_type{QWidget} : \c QWidget to replace in the MainWindow. <b>CANNOT</b> be <tt>null</tt>.
     *
     *  Class constructor.\n
     *  Accepts a \c QWidget* and replaces it (gets its geometry, position and parent).
     */
    QFileSelectionWidget(QWidget* widget_to_replace);
    ~QFileSelectionWidget();

    /*!
     *  \brief Gets the current text of the \c QLineEdit.
     *
     *  Gets the current text of the \c QLineEdit holding either the full path to the file selected, or just the name, depending on the current \c show_path_mode.
     *
     *  \return \format_return_type{QString} : \c QString with the text of the \c QLineEdit, that represents the file selected.
     *  \par Example of use
     *  \code
     *      // Example use of QFileSelectionWidget::text()
     *
     *          QFileSelectionWidget *qfsw = new QFileSelectionWidget(ui->widget_example);  // Our QFileSelectionWidget instance.
     *          ......
     *          QString qfsw_file_text = qfsw->text();                                      // After user interaction with it, we get the current QLineEdit text.
     *  \endcode
     */
    QString text();
    /*!
     *  \brief Gets the name of the file selected.
     *
     *  Gets the name of the file selected, ignoring the \c show_path_mode.
     *
     *  \return \format_return_type{QString} : \c QString with name of the file selected.
     *  \par Example of use
     *  \code
     *      // Example use of QFileSelectionWidget::name()
     *
     *          QFileSelectionWidget *qfsw = new QFileSelectionWidget(ui->widget_example);  // Our QFileSelectionWidget instance.
     *          ......
     *          QString qfsw_file_name = qfsw->name();                                      // After user interaction with it, we get the currently selected file's name.
     *  \endcode
     */
    QString name();
    /*!
     *  \brief Gets the full path of the file selected.
     *
     *  Gets the full path of the file selected, ignoring the \c show_path_mode.
     *
     *  \return \format_return_type{QString} : \c QString with full path of the file selected.
     *  \par Example of use
     *  \code
     *      // Example use of QFileSelectionWidget::path()
     *
     *          QFileSelectionWidget *qfsw = new QFileSelectionWidget(ui->widget_example);  // Our QFileSelectionWidget instance.
     *          ......
     *          QString qfsw_file_path = qfsw->path();                                      // After user interaction with it, we get the currently selected file's full path.
     *  \endcode
     */
    QString path();
    /*!
     *  \brief Gets the selection status of the widget.
     *
     *  Gets the selection status of the widget.
     *
     *  \return \format_return_type{bool} : \c bool with True if there is a file selected, and False if there's not.
     *  \par Example of use
     *  \code
     *      // Example use of QFileSelectionWidget::path()
     *
     *          QFileSelectionWidget *qfsw = new QFileSelectionWidget(ui->widget_example);  // Our QFileSelectionWidget instance.
     *          ......
     *          if(qfsw->selected()){
     *              ..
     *          }
     *  \endcode
     */
    bool selected();
    /*!
     * \brief Sets the currently selected file to the file pointed by \c new_path.
     *
     *  Sets the currently selected file to the file pointed by \c new_path.\n
     *  The file passes two tests:
     *  First, it passes the \c browse_extension_filter test. Here its extension will be tested against the currently accepted extensions defined in \c browse_extension_filter.\n
     *  Then, if it passes the first test, comes the existence test.\n
     *  The existence test is a requirement, you shall know beforehand if you ask for a file that must or must not exist.\n
     *  If the file does not pass a test, the function does nothing.
     *
     * \param new_path : \format_param_type{QString} : \c path pointing to the file that we want this \c QFileSelector to have selected.
     * \param required_existence : \format_param_type{bool} : \c bool expressing the existence status to check against \c new_path.
     * \return \format_return_type{bool} : \c bool representing if something was done or not.
     *  \todo Examples of use
     */
    bool select_file(const QString& new_path, bool required_existence);
    /*!
     *  \brief Tests a given file against the \c browse_extension_filter and returns the result.
     *
     *  Tests a given file against the \c browse_extension_filter and returns the result.\n
     *  Note that this does <i>NOT</i> check if the file exists or not, only checks if its extension is valid.\n
     *  Returns a \c bool representing the result of the test.
     *
     *  \param f : \format_param_type{QString} : Path or name of the file to test, <i>WITH EXTENSION</i>.
     *  \return \format_return_type{bool} : \c bool representing if the test passed (\c true) or not (\c false).
     *  \todo Examples of use
     */
    bool filter_file(const QString& f);
    /*!
     *  \brief Returns a \c QString with the name of the file referred by \c "path".
     *
     *  Returns a \c QString with the name of the file referred by \c "path".\n
     *  Due to the wild nature of the paths in different systems, this method tries to resolve what kind of path it was fed.\n
     *  If it's unable to infer what kind of path it is, fails.
     *
     *  \param path : \format_param_type{QString} : Path of the file to extract the name from.
     *  \return \format_return_type{QString} : \c QString with just the name of the file, or , or the original \c path on failure.
     *  \todo Examples of use
     */
    QString path_to_name(const QString& path);

    /*!
     * \brief 'Flips' this instances' <tt>show_path_mode</tt> property to the opposite.
     *
     *  This method 'flips' this instances' actual <tt>show_path_mode</tt> property, whatever it may be.
     */
    void conf_flip_show_path_mode();
    /*!
     * \brief Sets this instances' <tt>show_path_mode</tt> property to \c "new_show_path".
     *
     *  Sets this instances' <tt>show_path_mode</tt> property to \c "new_show_path".\n
     *  If the <tt>show_path_mode</tt> property is already \c "new_show_path", nothing happens.
     *
     * \param new_show_path : \format_param_type{bool} : \c bool with the new <tt>show_path_mode</tt> property.
     */
    void conf_set_show_path_mode(bool new_show_path);
    /*!
     * \brief Gets the current <tt>show_path_mode</tt> property.
     *
     *  Gets the current <tt>show_path_mode</tt> property.
     *
     * \return \format_return_type{bool} : \c bool representing the current <tt>show_path_mode</tt> property.
     */
    bool conf_get_show_path_mode();
    /*!
     * \brief 'Flips' this instances' \c editable_path_state property to the opposite.
     *
     *  Sets this instances' \c QLineEdit \c editable_path_state to the opposite value that it had before.
     */
    void conf_flip_editable_path_state();
    /*!
     *  \brief Sets the \c editable_path_state property.
     *
     *  Sets the \c editable_path_state of the \c QLineEdit containing the path or name of the file.
     *
     *  \param new_state : \format_param_type{bool} : \c bool representing the new state of the \c editable_path_state property.
     */
    void conf_set_editable_path_state(bool new_state);
    /*!
     *  \brief Gets the \c editable_path_state property.
     *
     *  Gets the \c editable_path_state property of the \c QLineEdit containing the path or name of the file selected.
     *
     *  \return \format_return_type{bool} : \c bool representing the current state of the \c editable_path_state property.
     */
    bool conf_get_editable_path_state();
    /*!
     * \brief 'Flips' this instances' <tt>file_open_save_mode</tt> property to the opposite.
     *
     *  This method 'flips' this instances' actual <tt>file_open_save_mode</tt> property, whatever it may be.
     */
    void conf_flip_file_open_save_mode();
    /*!
     * \brief Sets this instances' <tt>file_open_save_mode</tt> property to \c "new_file_mode".
     *
     *  Sets this instances' <tt>file_open_save_mode</tt> property to \c "new_file_mode".\n
     *  If the <tt>file_open_save_mode</tt> property is already \c "new_file_mode", nothing happens.
     *
     * \param new_file_mode : \format_param_type{bool} : \c bool with the new <tt>file_open_save_mode</tt> property.
     */
    void conf_set_file_open_save_mode(bool new_file_mode);
    /*!
     * \brief Gets the current <tt>file_open_save_mode</tt> property.
     *
     *  Gets the current <tt>file_open_save_mode</tt> property.
     *
     * \return \format_return_type{bool} : \c bool representing the current <tt>file_open_save_mode</tt> property.
     */
    bool conf_get_file_open_save_mode();
    /*!
     * \brief 'Flips' this instances' <tt>remember_last_path</tt> property to the opposite.
     *
     *  This method 'flips' this instances' actual <tt>remember_last_path</tt> property, whatever it may be.
     */
    void conf_flip_remember_last_path();
    /*!
     * \brief Sets this instances' <tt>remember_last_path</tt> property to \c "new_remember".
     *
     *  Sets this instances' <tt>remember_last_path</tt> property to \c "new_remember".\n
     *  If the <tt>remember_last_path</tt> property is already \c "new_remember", nothing happens.
     *
     * \param new_remember : \format_param_type{bool} : \c bool with the new <tt>remember_last_path</tt> property.
     */
    void conf_set_remember_last_path(bool new_remember);
    /*!
     * \brief Gets the current <tt>remember_last_path</tt> property.
     *
     *  Gets the current <tt>remember_last_path</tt> property.
     *
     * \return \format_return_type{bool} : \c bool representing the current <tt>remember_last_path</tt> property.
     */
    bool conf_get_remember_last_path();
    /*!
     * \brief Sets this instances' <tt>basepath</tt> property to \c "new_basepath".
     *
     *  Sets this instances' <tt>basepath</tt> property to \c "new_basepath".\n
     *  If the <tt>basepath</tt> property is already \c "new_basepath", nothing happens.
     *
     * \param new_basepath : \format_param_type{QString} : \c QString with the new <tt>basepath</tt> property.
     */
    void conf_set_basepath(QString new_basepath);
    /*!
     * \brief Gets the current <tt>basepath</tt> property.
     *
     *  Gets the current <tt>basepath</tt> property.
     *
     * \return \format_return_type{QString} : \c QString representing the current <tt>basepath</tt> property.
     */
    QString conf_get_basepath();
    /*!
     * \brief Sets this instances' <tt>browse_extension_filter</tt> property to \c "new_browse_extension_filter".
     *
     *  Sets this instances' <tt>browse_extension_filter</tt> property to \c "new_browse_extension_filter".\n
     *  If the <tt>browse_extension_filter</tt> property is already \c "new_browse_extension_filter", nothing happens.
     *
     * \param new_browse_extension_filter : \format_param_type{QString} : \c QString with the new <tt>browse_extension_filter</tt> property.
     */
    void conf_set_browse_extension_filter(QString new_browse_extension_filter);
    /*!
     * \brief Gets the current <tt>browse_extension_filter</tt> property.
     *
     *  Gets the current <tt>browse_extension_filter</tt> property.
     *
     * \return \format_return_type{QString} : \c QString representing the current <tt>browse_extension_filter</tt> property.
     */
    QString conf_get_browse_extension_filter();
    /*!
     * \brief 'Flips' this instances' \c accepts_drops property to the opposite.
     *
     *  Sets this instances' \c accepts_drops to the opposite value that it had before.
     */
    void conf_flip_accepts_drops();
    /*!
     * \brief Sets this instances' <tt>accepts_drops</tt> property to \c "new_accepts_drops".
     *
     *  Sets this instances' <tt>accepts_drops</tt> property to \c "new_accepts_drops".\n
     *  If the <tt>accepts_drops</tt> property is already \c "new_accepts_drops", nothing happens.
     *
     * \param new_accepts_drops : \format_param_type{bool} : \c bool with the new <tt>accepts_drops</tt> property.
     */
    void conf_set_accepts_drops(bool new_accepts_drops);
    /*!
     * \brief Gets the current <tt>accepts_drops</tt> property.
     *
     *  Gets the current <tt>accepts_drops</tt> property.
     *
     * \return \format_return_type{bool} : \c bool representing the current <tt>accepts_drops</tt> property.
     */
    bool conf_get_accepts_drops();

signals:
    void selected_file(QString file_path);

protected:
    void dragEnterEvent(QDragEnterEvent *event);
    void dropEvent(QDropEvent *event);

private:
    QString browse_caption = "Select a file";
    QString current_file_path = "";
    QString basepath = "";
    QString browse_extension_filter;
    Ui::QFileSelectionWidget *ui;
    bool show_path = true;
    bool file_open_save_mode = true; //true = open
    bool remember_last_path = true;
    bool selected_bool = false;

    void refresh_text();

private slots:
    void browse_file();
};

#endif // QFILESELECTIONWIDGET_H
