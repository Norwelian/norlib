#ifndef NORLIB_H
#define NORLIB_H

#include <QDebug>
#include <QByteArray>
#include <QCryptographicHash>
#include <QFile>
#include <QtMath>
#include <QTableWidget>

/*!
 *  \mainpage NorLib documentation
 *  \section intro Introduction
 *
 *  Welcome to the documentation of NorLib.\n\n
 *  NorLib is an auxiliar library that contains various functions as well as custom \c Qt classes, like \c QDialogs and \c QWidgets.\n
 *  Below you will find a list that contains the actual functions and classes, with definitions and examples.
 *
 *  \n
 *  \section funcs 1. Functions
 *
 *  In this section, the auxiliar functions are listed, grouped by their respective <a href="modules.html">\ccb_{module}</a>:\n
 *  - <a href="group___print.html">\ccb_{Print}</a>
 *      - \c NorLib::print_QStringList()
 *      - \c NorLib::print_QMap()
 *      .
 *  .
 *  - <a href="group___files.html">\ccb_{Files}</a>
 *      - \c NorLib::file_hash_generate()
 *      - \c NorLib::file_hash_dump()
 *      - \c NorLib::file_hash_check()
 *      .
 *  .
 *
 *  - <a href="group___format.html">\ccb_{Format}</a>
 *      - \c NorLib::format_number()
 *      .
 *  .
 *  - <a href="group___options.html">\ccb_{Options}</a>
 *      - \c NorLib::option_set()
 *      - \c NorLib::option_get()
 *      - \c NorLib::options_set()
 *      - \c NorLib::options_get()
 *      .
 *  .
 *  - <a href="group___style.html">\ccb_{Style}</a>
 *      - \c NorLib::style_dark()
 *      .
 *  .
 *
 *  \n
 *  \section qdialogs 2. QDialogs
 *  In this section, the different \format_param_type{QDialog} inheriting classes are listed.\n
 *  The idea behind a \c QDialog is to call if from your code to achieve something from it, normally some kind of user input.
 *
 *  - <a href="group___q_dialogs.html">\ccb_{Classes QDialog}</a>:
 *      - \c QOptionsDialog
 *          - \ccb_{Constructor}:
 *              - \c QOptionsDialog::QOptionsDialog()
 *          .
 *      .
 *  .
 *
 *  \n
 *  \section qwidgets 3. QWidgets
 *  In this section, the different \format_param_type{QWidget} inheriting classes are listed.\n
 *  The idea behind a \c QWidget is to use it in your GUI as a control.\n
 *  For example, to ask for user input, or to add some functionality to a program.
 *
 *  - <a href="group___q_widgets.html">\ccb_{Classes QWidget}</a>:
 *      - \c QFileSelectionWidget
 *          - \ccb_{Constructor}:
 *              - \c QFileSelectionWidget::QFileSelectionWidget()
 *          .
 *          - \ccb_{Functions}:
 *              - \c QFileSelectionWidget::select_file()
 *              - \c QFileSelectionWidget::filter_file()
 *              - \c QFileSelectionWidget::path_to_name()
 *          .
 *          - \ccb_{Properties getters/setters}:
 *              - \c QFileSelectionWidget::text()
 *              - \c QFileSelectionWidget::name()
 *              - \c QFileSelectionWidget::path()
 *          .
 *          - \ccb_{Configuration getters/setters}:
 *              - \ccb_{show_path_mode}:
 *                  - \c QFileSelectionWidget::conf_flip_show_path_mode()
 *                  - \c QFileSelectionWidget::conf_set_show_path_mode()
 *                  - \c QFileSelectionWidget::conf_get_show_path_mode()
 *              .
 *              - \ccb_{editable_path_state}:
 *                  - \c QFileSelectionWidget::conf_flip_editable_path_state()
 *                  - \c QFileSelectionWidget::conf_set_editable_path_state()
 *                  - \c QFileSelectionWidget::conf_get_editable_path_state()
 *              .
 *              - \ccb_{file_open_save_mode}:
 *                  - \c QFileSelectionWidget::conf_flip_file_open_save_mode()
 *                  - \c QFileSelectionWidget::conf_set_file_open_save_mode()
 *                  - \c QFileSelectionWidget::conf_get_file_open_save_mode()
 *              .
 *              - \ccb_{remember_last_path}:
 *                  - \c QFileSelectionWidget::conf_flip_remember_last_path()
 *                  - \c QFileSelectionWidget::conf_set_remember_last_path()
 *                  - \c QFileSelectionWidget::conf_get_remember_last_path()
 *              .
 *              - \ccb_{accepts_drops}:
 *                  - \c QFileSelectionWidget::conf_flip_accepts_drops()
 *                  - \c QFileSelectionWidget::conf_set_accepts_drops()
 *                  - \c QFileSelectionWidget::conf_get_accepts_drops()
 *              .
 *          .
 *      .
 *  .
 *
 *  \n
 *  \section qobjects 4. QObjects
 *  In this section, the different \format_param_type{QObject} inheriting classes are listed.\n
 *  A class inherits QObject when it wants the \c SIGNAL/SYMBOL .
 *
 *  - <a href="group___q_objects.html">\ccb_{Classes QObject}</a>:
 *      - \c QProcessQueue
 *          - \ccb_{Constructor}:
 *              - \c QProcessQueue::QProcessQueue()
 *          .
 *          - \ccb_{Functions}:
 *              - \c QProcessQueue::start()
 *              - \c QProcessQueue::stop()
 *              - \c QProcessQueue::queue_add()
 *              - \c QProcessQueue::queue_add_start()
 *          .
 *          - \ccb_{Properties getters/setters}:
 *              - \c QProcessQueue::set_shell_command()
 *              - \c QProcessQueue::process_started()
 *              - \c QProcessQueue::process_output()
 *              - \c QProcessQueue::queue_count()
 *              - \c QProcessQueue::queue_isEmpty()
 *          .
 *      .
 *  .
 *
 */

/*!
 *  \defgroup QWidgets Classes QWidget
 *  \brief Classes that inherit \c QWidget.
 */
/*!
 *  \defgroup QDialogs Classes QDialog
 *  \brief Classes that inherit \c QDialog.
 */
/*!
 *  \defgroup QObjects Classes QObject
 *  \brief Classes that inherit \c QObject.
 */
/*!
 *  \namespace NorLib
 *  \brief All the auxiliar functions are located in this namespace.
 *
 *  The functions are organized in <a href="modules.html">\ccb_{modules}</a>, and right now there are 5 modules of functions:
 *  - <a href="group___print.html">\ccb_{Print}</a>
 *  - <a href="group___files.html">\ccb_{Files}</a>
 *  - <a href="group___format.html">\ccb_{Format}</a>
 *  - <a href="group___options.html">\ccb_{Options}</a>
 *  - <a href="group___style.html">\ccb_{Style}</a>
 *  .
 *  You can distinguish easily which function belongs to which section by the initial part of its name.\n
 *
 */
namespace NorLib {

    // Print
    /*!
     *  \defgroup Print Functions Print
     *  \brief Functions that return a \c QString representation of some other object.
     *
     *  Functions that return a \c QString representation of some other object.
     *
     *  @{
     */

    /*!
     *  \brief Returns a \c QString representation of the given \c QStringList.
     *
     *  Returns a \c QString representation of the given \c QStringList.\n
     *  The returned \c QString is in the form: \c "[ 'item1' 'item2' ... ]"
     *
     *  \param qsl : \format_param_type{QStringList} : The \c QStringList to print.
     *  \return \format_return_type{QString} : \c QString containing the string representation asked for, or an empty \c QString on failure.
     *  \par Example outputs
     *  \code
     *      //Example returns of NorLib::print_QStringList()
     *
     *          //Valid inputs
     *          NorLib::print_QStringList( ["hello", "world"] );      // QString( "[ 'hello' 'world' ]" )
     *          NorLib::print_QStringList( ["1234", "56", "7890"] );  // QString( "[ '123456' '56' '7890' ]" )
     *          NorLib::print_QStringList( ["hi"] );                  // QString( "[ 'hi' ]" )
     *
     *          //Invalid inputs
     *          NorLib::print_QStringList( ["abc", ""] );  // QString()
     *          NorLib::print_QStringList( [""] );         // QString()
     *          NorLib::print_QStringList( [] );           // QString()
     *  \endcode
     */
    QString print_QStringList(const QStringList& qsl);
    /*!
     *  \overload
     *  \n Overloaded function: \c NorLib::print_QStringList(const QStringList& qsl)
     *  \param qsl : \format_param_type{QStringList*} : The \c QStringList* to print.
     */
    QString print_QStringList(QStringList *qsl);

    /*!
     *  \brief Returns a \c QString representation of the given \c QMap<QString, QStringList>.
     *
     *  Returns a \c QString representation of the given \c QMap<QString, QStringList>.\n
     *
     *  \param map : \format_param_type{QMap<QString, QStringList>} : The \c QMap<QString, QStringList> to print.
     *  \return \format_return_type{QString} : \c QString containing the string representation asked for, or an empty \c QString on failure.
     *  \par Example outputs
     *  \code
     *      //Example returns of NorLib::print_QMap()
     *
     *          //Valid inputs
     *          NorLib::print_QMap( {
     *                              "hello1" : ["world11", "world12", "world13"],   // QString( "[ 'hello1' ][ 'world11' 'world12' 'world13' ]
     *                              "hello2" : ["world21", "world22", "world23"],   //           [ 'hello2' ][ 'world21' 'world22' 'world23' ]
     *                              "hello3" : ["world31", "world32", "world33"],   //           [ 'hello3' ][ 'world31' 'world32' 'world33' ]" )
     *                              } );
     *
     *          //Invalid inputs
     *          NorLib::print_QMap( {} );   // QString()
     *  \endcode
     */
    QString print_QMap(const QMap<QString, QStringList>& map);
    /*!
     *  \overload
     *  \n Overloaded function: \c NorLib::print_QMap(const QMap<QString, QStringList>& m)
     *  \param map : \format_param_type{QMap<QString, QStringList>*} : The \c QMap<QString, QStringList>* to print.
     */
    QString print_QMap(QMap<QString, QStringList> *map);
    /*!
     *  @}
     */

    //Files
    /*!
     *  \defgroup Files Functions File
     *  \brief Functions that process files in some way.
     *
     *  Functions that process files in some way.\n
     *  On some of them, you will be asked for a \c QCryptographicHash::Algorithm, below you will find a list of the most used ones, and <a href="http://doc.qt.io/qt-5/qcryptographichash.html#Algorithm-enum"><b>here</b></a> is a link to the full <tt>QCryptographicHash::Algorithm enum</tt> documentation.
     *
     * 	\par Most used algorithms
     *  - \format_param_type{QCryptographicHash::Md5}
     *  - \format_param_type{QCryptographicHash::Sha256}
     *  - \format_param_type{QCryptographicHash::Sha512}
     *  - \format_param_type{QCryptographicHash::Sha3_256}
     *  - \format_param_type{QCryptographicHash::Sha3_512}
     *  .
     *
     *  @{
     */

    /*!
     *  \brief Returns a \c QByteArray representing the cryptographic hash of the given file.
     *
     *  Returns a \c QByteArray representing the cryptographic hash of the given file.
     *
     *  \param algo : \format_param_type{QCryptographicHash::Algorithm} : Hashing algorithm to use.
     *  \param file_path : \format_param_type{QString} : File to calculate the hash from.
     *  \return \format_return_type{QByteArray} : \c QByteArray containing the cryptographic hash of the file given, or an empty \c QByteArray on failure.
     *
     *  \par Example of use
     *  \code
     *      //Example of use of NorLib::file_hash_generate
     *
     *          QByteArray test_hash_file = NorLib::file_hash_generate(QCryptographicHash::Algorithm::Sha3_512, "test_file.data");
     *  \endcode
     */
    QByteArray file_hash_generate(QCryptographicHash::Algorithm algo, const QString& file_path);
    /*!
     *  \brief Generates a hash and dumps it to disk.
     *
     *  This method generates a hash for the file given, and saves it in disk, in a file whose name is <tt>"file_path".append("hash_extension")</tt>.
     *  The return indicates the success/failure of the hash output file creation.
     *
     *
     *  \param algo : \format_param_type{QCryptographicHash::Algorithm} : Hashing algorithm to use.
     *  \param file_path : \format_param_type{QString} : File to calculate the hash from.
     *  \param hash_extension : \format_param_type{QString} : Extension to append to the name of the file for the output.
     *  \return \format_return_type{bool} : \c bool representing the success or failure on dumping the hash file to disk.
     *
     *  \par Example of use
     *  \code
     *      //Example of use of NorLib::file_hash_generate
     *
     *          // Default hash_extension
     *          if(NorLib::file_hash_dump(QCryptographicHash::Algorithm::Sha3_512, "test_file.data"))
     *              qDebug() << "SUCCESS!";
     *          else
     *              qDebug() << "ERROR";
     *
     *          // Custom hash_extension
     *          if(NorLib::file_hash_dump(QCryptographicHash::Algorithm::Sha3_512, "test_file.data", ".hash"))
     *              qDebug() << "SUCCESS!";
     *          else
     *              qDebug() << "ERROR";
     *   \endcode
     */
    bool file_hash_dump(QCryptographicHash::Algorithm algo, QString file_path, const QString& hash_extension = ".chksum");
    /*!
     *  \todo_all{Documentation and examples}
     */
    bool file_hash_check(QCryptographicHash::Algorithm algo, const QString& file_path, const QByteArray& expected_hash);
    /*!
     *  \todo_all{Documentation and examples}
     */
    bool file_hash_check(QCryptographicHash::Algorithm algo, QString file_path, const QString& hash_extension = ".chksum");
    /*!
     *  @}
     */

    //Format
    /*!
     *  \defgroup Format Functions Format
     *  \brief Functions that define some sort of conversion between formats.
     *  @{
     */

    /*!
     *  \brief Returns a \c QString representing the number given.
     *
     *  Returns a \c QString representing the number given.\n
     *  The output format has \c ',' (comma) as the three-digit separators and \c '.' (dot) as decimal separator.\n
     *  The precision is 2 decimal places.
     *
     *  \param num : \format_param_type{QString} : Number to format.
     *  \return \format_return_type{QString} : \c QString containing the formatted number, or an empty \c QString on failure.
     *  \par Example outputs
     *  \code
     *      // Example returns of NorLib::format_number()
     *
     *          // Valid inputs
     *          NorLib::format_number( "19782987.0723" );   // QString( "19,782,987.07" )
     *          NorLib::format_number( "-19782987.0723" );  // QString( "-19,782,987.07" )
     *          NorLib::format_number( "0" );               // QString( "0.00" )
     *          NorLib::format_number( "5002" );            // QString( "5,002.00" )
     *
     *          // Invalid inputs
     *          NorLib::format_number( "a" );  // QString()
     *          NorLib::format_number( "" );   // QString()
     *  \endcode
     */
    QString format_number(QString num);
    /*!
     *  \overload
     *  \n Overloaded function: \c NorLib::format_number(QString num)
     *  \param num : \format_param_type{QString*} : Number to format.
     */
    QString format_number(QString *num);
    /*!
     *  \overload
     *  \n Overloaded function: \c NorLib::format_number(QString num)
     *  \param num : \format_param_type{double} : Number to format.
     */
    QString format_number(double num);
    /*!
     *  \todo_all{Documentation and examples}
     */
    QList<QStringList> format_qtablewidget(QTableWidget *tw);
    /*!
     *  @}
     */
    //Options
    /*!
     *  \defgroup Options Functions Option
     *  \brief Functions to manage options with persistence on disk and a custom format.
     *  @{
     */

    /*!
     *  \brief Sets the option \c "option_to_set" to \c "new_value" on disk.
     *
     *  Sets the option given by \c "option_name" to \c "new_value" on disk.\n
     *  If \c create is \c true, the option will be created if it doesn't exist already, if it's \c false, it won't be created.
     *
     *  \param optfile_path : \format_param_type{QString} : File on disk where the options are stored.
     *  \param option_to_set : \format_param_type{QString} : Name of the option to set.
     *  \param new_value : \format_param_type{QString} : New value for the option to get on success.
     *  \param create : \format_param_type{bool} : Create the option if it doesn't exists on disk at the moment.
     *  \return \format_return_type{bool} : \c bool indicating the success of the operation.
     *  \par Example of use
     *  \code
     *      // Example use of NorLib::option_set()
     *
     *          bool success = NorLib::option_set("options.opt", "option1", "value1", false);
     *          if(success)
     *              printf("option1 successfully set");
     *          else
     *              printf("option1 does not exist");
     *  \endcode
     */
    bool option_set(const QString& optfile_path, const QString& option_to_set, const QString& new_value, bool create);
    /*!
    *  \brief Gets the option \c "option_to_get" from disk.
    *
    *  Gets the option given by \c "option_to_get" from disk.
    *
    *  \param optfile_path : \format_param_type{QString} : File on disk where the options are stored.
    *  \param option_to_get : \format_param_type{QString} : Name of the option to get.
    *  \return \format_return_type{QString} : \c QString with the value of the option got from disk, or an empty \c QString on failure.
    *  \par Example of use
    *  \code
    *       // Example use of NorLib::option_get()
    *
    *           QString value = NorLib::option_get("options.opt", "option1");   // value = "value1"
    *  \endcode
    */
    QString option_get(const QString& optfile_path, const QString& option_to_get);
    /*!
     *  \todo_all{Documentation and examples}
     */
    bool options_set(const QString& optfile_path, QMap<QString, QString> *options_and_defaults);
    /*!
     *  \todo_all{Documentation and examples}
     */
    QMap<QString, QString> options_get(const QString& optfile_path);
    /*!
     *  \todo_all{Documentation and examples}
     */
    QString options_base64_encode(const QString& decoded_options);
    /*!
     *  \todo_all{Documentation and examples}
     */
    QString options_base64_decode(const QString& encoded_options);
    /*!
     *  @}
     */

    //Style
    /*!
     *  \defgroup Style Functions Style
     *  \brief Functions to retrieve custom stylesheets for Qt.
     *  @{
     */
    /*!
     *  \todo_all{Documentation and examples}
     */
    QString style_dark();
    /*!
     *  @}
     */
}

#endif // NORLIB_H

