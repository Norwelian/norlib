#ifndef QPROCESSQUEUE_H
#define QPROCESSQUEUE_H

#include <QProcess>
#include <QQueue>
#include <QMap>
#include <QTimer>

#include <norlib.h>

/*!
*  \ingroup QObjects
*  \class QProcessQueue
*  \brief Provides a reactor-like enviroment to execute processes in strict order in a queue.
*
*   Provides a reactor-like enviroment to execute processes in strict order in a queue.
*
*  \todo Documentation and examples
*/
class QProcessQueue : public QObject
{
    Q_OBJECT

public:
    /*!
     *  \todo_all{Documentation and examples}
     */
    QProcessQueue();
    ~QProcessQueue();

    /*!
     *  \todo_all{Documentation and examples}
     */
    void start();
    /*!
     *  \todo_all{Documentation and examples}
     */
    void stop();
    /*!
     *  \todo_all{Documentation and examples}
     */
    void queue_add(const QStringList& argz);
    /*!
     *  \todo_all{Documentation and examples}
     */
    void queue_add_start(const QStringList& argz);
    /*!
     *  \todo_all{Documentation and examples}
     */
    void set_shell_command(QString new_cmd = "cmd.exe");
    /*!
     *  \todo_all{Documentation and examples}
     */
    bool process_started();
    /*!
     *  \todo_all{Documentation and examples}
     */
    QString process_output();
    /*!
     *  \todo_all{Documentation and examples}
     */
    int queue_count();
    /*!
     *  \todo_all{Documentation and examples}
     */
    bool queue_isEmpty();

private:
    QProcess *proc;
    QQueue<QStringList> *ext_queue;
    QTimer *timer;
    QString proc_output; // output from process is here.
    QString shell_command = "cmd.exe";
    bool process_status = false;
    bool queue_isempty = true;

signals:
    void current_proc_end();
    void all_proc_end();

private slots:
    void queue_tick();
    void proc_read();
    void end_proc(int e);
};

#endif // QPROCESSQUEUE_H
