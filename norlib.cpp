#include "norlib.h"

#include <utility>

inline void initMyResource() { Q_INIT_RESOURCE(style); }
// Print
QString NorLib::print_QStringList(const QStringList& qsl){
    QString deb;
    if(!qsl.isEmpty()){
        deb.append("[ ");
        for(int i = 0; i < qsl.count(); i++){
            if(qsl.at(i).isEmpty())
                return QString();
            deb.append("'" + qsl.at(i) + "' ");
        }
        deb.append("]");
    }
    return deb;
}
QString NorLib::print_QStringList(QStringList *qsl){
    return NorLib::print_QStringList(*qsl);
}
QString NorLib::print_QMap(const QMap<QString, QStringList>& m){
    QString ret;
    auto keyz = m.keys();
    auto valuez = m.values();
    for(int i = 0; i < valuez.count(); i++){
        ret.append(NorLib::print_QStringList(QStringList() << keyz.at(i)).append(NorLib::print_QStringList(valuez.at(i))).append("\n"));
    }
    if(!ret.isEmpty())
        ret.truncate(ret.count()-1);
    return ret;
}
QString NorLib::print_QMap(QMap<QString, QStringList> *map){
    return NorLib::print_QMap(*map);
}

//Files
QByteArray NorLib::file_hash_generate(QCryptographicHash::Algorithm algo, const QString& file_path){
    QFile f(file_path);
    auto ret = QByteArray();
    if (f.open(QFile::ReadOnly)) {
        QCryptographicHash hash(algo);
        if (hash.addData(&f)) {
            ret = hash.result().toHex();
        }
        f.close();
    }
    return ret;
}
bool NorLib::file_hash_dump(QCryptographicHash::Algorithm algo, QString file_path, const QString& hash_extension){
    QByteArray a = NorLib::file_hash_generate(algo, file_path);
    QString file_out_path = file_path.append(hash_extension);
    bool ret = false;
    if(!a.isNull()){
        QFile f(file_out_path);
        if (f.open(QFile::WriteOnly | QFile::Truncate)) {
            f.write(a);
            f.close();
            ret = true;
        }
    }
    return ret;
}
bool NorLib::file_hash_check(QCryptographicHash::Algorithm algo, const QString& file_path, const QByteArray& expected_hash){
    QByteArray a = NorLib::file_hash_generate(algo, file_path);
    if(a.isEmpty())
        return false;
    return a == expected_hash;
}
bool NorLib::file_hash_check(QCryptographicHash::Algorithm algo, QString file_path, const QString& hash_extension){
    QByteArray a = NorLib::file_hash_generate(algo, file_path);
    QString hash_file_path = file_path.append(hash_extension);
    bool ret = false;
    if(!a.isNull()){
        QFile f(hash_file_path);
        if (f.open(QFile::ReadOnly)) {
            if(f.readLine() == a)
                ret = true;
            f.close();
        }
    }
    return ret;
}

//Format
QString NorLib::format_number(QString num){
    bool is_number;
    bool negative = false;
    num.toDouble(&is_number);
    if(!is_number)
        return QString();

    if(num.at(0) == "-"){
        negative = true;
        num = num.remove(0,1);
    }
    QStringList aux_num_parts;
    QString aux_whole;
    QString aux_decimal;
    if(!num.contains(".")){
        aux_whole = num;
        aux_decimal = "00";
    } else {
         aux_num_parts = num.split(".");
         aux_whole = aux_num_parts.at(0);
         aux_decimal = aux_num_parts.at(1);
    }

    if(aux_whole.isEmpty())
        aux_whole = "0";

    if(aux_decimal.count() > 2)
        aux_decimal.remove(2, 1024);
    else if(aux_decimal.count() == 1)
        aux_decimal.append("0");
    else if(aux_decimal.count() == 0)
        aux_decimal.append("00");
    QString new_whole;
    if(aux_whole.count() > 3){
        std::reverse(aux_whole.begin(), aux_whole.end());
        int tmp = qFloor(aux_whole.count()/3.);
        for(int i = 0; i < tmp; i++){
            for(int j = 0; j < 3; j++){
                new_whole.append(aux_whole.at(0));
                aux_whole = aux_whole.remove(0, 1);
            }
            new_whole.append(",");
        }
        new_whole.append(aux_whole);
        std::reverse(new_whole.begin(), new_whole.end());
    } else
        new_whole = aux_whole;
    return (negative?QString("-"):QString()).append(new_whole).append(".").append(aux_decimal);
}
QString NorLib::format_number(QString *num){
    return NorLib::format_number(*num);
}
QString NorLib::format_number(double num){
    return NorLib::format_number(QString::number(num, 'f', 2));
}

QList<QStringList> NorLib::format_qtablewidget(QTableWidget * tw) {
    QList<QStringList*> ret;
    for(int c = 0; c < tw->columnCount(); c++){
        ret.append(new QStringList());
        for(int r = 0; r < tw->rowCount(); r++){
            ret.at(c)->append(tw->item(r,c)->text());
        }
    }
}

//Options
bool NorLib::option_set(const QString& optfile_path, const QString& option_to_set, const QString& new_value, bool create){
    auto f = new QFile(optfile_path);
    if(!f->exists()){
        if(f->open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)){
            f->write("");
            f->close();
        } else
            return false;
    }
    QString buff;
    int index_aux;
    if(f->open(QIODevice::ReadOnly | QIODevice::Text)){
        QString entire_f = f->readAll();
        f->reset();
        QTextStream in(f);
        bool found = false;
        while(!in.atEnd()){
            buff = in.readLine();
            index_aux = buff.indexOf(':');
            if(index_aux != -1){
                QStringRef name = buff.leftRef(index_aux - 2 + 1);
                if(name == option_to_set){
                    entire_f.replace(buff, option_to_set + "/:\\" + new_value);
                    found = true;
                    break;
                }
            }
        }

        if(f->isOpen())
            f->close();

        if(!found){
            if(create)
                entire_f.append(option_to_set + "/:\\" + new_value + "\n");
            else
                return false;
        }

        if(f->open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)){
            if(f->write(entire_f.toStdString().c_str()) == qstrlen(entire_f.toStdString().c_str())){
                if(f->isOpen())
                    f->close();
                return true;
            }
        }
        if(f->isOpen())
            f->close();
    }
    return false;
}
QString NorLib::option_get(const QString& optfile_path, const QString& option_to_get){
    auto f = new QFile(optfile_path);
    auto ret = QString();
    if(f->exists()){
        QString buff;
        int index_aux;
        if(f->open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream in(f);
            while(!in.atEnd()){
                buff = in.readLine();
                index_aux = buff.indexOf("/:\\");
                if(index_aux != -1){
                    QStringRef name = buff.leftRef(index_aux - 1 + 1);
                    QString value = buff.right(buff.count() - (index_aux + 3));
                    if(name == option_to_get){
                        ret.append(value);
                        break;
                    }
                }
            }
            if(f->isOpen())
                f->close();
        }
    }
    return ret;
}
bool NorLib::options_set(const QString& optfile_path, QMap<QString, QString> *options_and_values){
    auto f = new QFile(optfile_path);
    if(!options_and_values->isEmpty() && f->open(QIODevice::WriteOnly | QIODevice::Truncate | QIODevice::Text)){
        auto keyz = options_and_values->keys();
        auto valuez = options_and_values->values();
        QString aux;
        for(int i = 0; i < keyz.count(); i++){
            aux.append(keyz.at(i)).append("/:\\").append(valuez.at(i)).append("\n");
        }
        f->write(aux.toStdString().c_str());
        f->close();
        return true;
    }
    return false;
}
QMap<QString, QString> NorLib::options_get(const QString& optfile_path){
    auto f = new QFile(optfile_path);
    auto ret = QMap<QString, QString>();
    if(f->exists()){
        QString buff;
        int index_aux;
        if(f->open(QIODevice::ReadOnly | QIODevice::Text)){
            QTextStream in(f);
            while(!in.atEnd()){
                buff = in.readLine();
                index_aux = buff.indexOf("/:\\");
                if(index_aux != -1){
                    ret.insert(buff.left(index_aux - 1 + 1), buff.right(buff.count() - (index_aux + 3)));
                }
            }
            if(f->isOpen())
                f->close();
        }
    }
    return ret;
}
QString NorLib::options_base64_encode(const QString& decoded_options){
    QByteArray arr;
    arr.append(decoded_options);
    return arr.toBase64();
}
QString NorLib::options_base64_decode(const QString& encoded_options){
    QByteArray arr;
    arr.append(encoded_options);
    return arr.fromBase64(arr);
}

QString NorLib::style_dark(){
    initMyResource();
    QFile f(":qdarkstyle/style.qss");

    if(f.exists() && f.open(QFile::ReadOnly | QFile::Text)){
        QTextStream ts(&f);
        return ts.readAll();
    }

    return QString();
}
