#include "qfileselectionwidget.h"
#include "ui_qfileselectionwidget.h"

QFileSelectionWidget::QFileSelectionWidget(QWidget* widget_to_replace) :
    QWidget(),
    ui(new Ui::QFileSelectionWidget)
{
    if(widget_to_replace){
        ui->setupUi(this);

        conf_set_editable_path_state(false);
        conf_set_browse_extension_filter("");

        setGeometry(widget_to_replace->geometry());
        setBaseSize(widget_to_replace->baseSize());
        setParent(widget_to_replace->parentWidget());

        connect(ui->pushButton, SIGNAL(clicked()), this, SLOT(browse_file()));
    }
}
QFileSelectionWidget::~QFileSelectionWidget()
{
    delete ui;
}

// FUNCTIONS
QString QFileSelectionWidget::text(){
    return ui->lineEdit->text();
}
QString QFileSelectionWidget::name(){
    return path_to_name(current_file_path);
}
QString QFileSelectionWidget::path(){
    return current_file_path;
}
bool QFileSelectionWidget::selected(){
    return selected_bool;
}
bool QFileSelectionWidget::select_file(const QString& new_path, bool required_existence){
    if(!new_path.isEmpty() && filter_file(new_path) && !(required_existence xor QFile(new_path).exists())){
        current_file_path = new_path;
        refresh_text();
        selected_bool = true;
        return true;
    }
    selected_bool = false;
    return false;
}
bool QFileSelectionWidget::filter_file(const QString& f){
    bool ret = true;
    if(browse_extension_filter.contains("*.*"))
        return ret;
    auto a = browse_extension_filter.split(";;");
    auto list_of_extensions = QList<QString>();
    foreach(const QString &i, a){
        list_of_extensions.append(i.split("*.").last());
    }

    if(!list_of_extensions.contains(f.split(".").last()))
        ret = false;
    //qDebug() << list_of_extensions << f.split(".").last();
    return ret;
}
QString QFileSelectionWidget::path_to_name(const QString& path){
    if(path.isEmpty()){
        return QString();
    }
    char separator;
    if(path.contains("/"))
        separator = '/';
    else if(path.contains("\\"))
        separator = '\\';
    else
        return path;

    auto aux_list = path.split(separator);
    auto ret_name = aux_list.last();
    aux_list.removeLast();

    return ret_name;
}

// DRAG AND DROP

void QFileSelectionWidget::dragEnterEvent(QDragEnterEvent *event)
{
    bool accepted = true;
    QList<QUrl> urls = event->mimeData()->urls();
    if (!urls.isEmpty()){
        for(int i = 0; i < urls.count(); i++){
            QString fileName = urls.at(i).toLocalFile();
            if(!filter_file(fileName)){
                accepted = false;
                break;
            }
        }
    }
    if(accepted)
        event->acceptProposedAction();
}

void QFileSelectionWidget::dropEvent(QDropEvent *event)
{
    QList<QUrl> urls = event->mimeData()->urls();
    if (!urls.isEmpty()){
        for(int i = 0; i < urls.count(); i++){
            QString fileName = urls.at(i).toLocalFile();
            if(select_file(fileName, true))
                emit selected_file(fileName);
        }
    }
}

// SLOTS
void QFileSelectionWidget::browse_file(){
    QString file_name;
    if(file_open_save_mode)
        file_name = QFileDialog::getOpenFileName(this, browse_caption, (!current_file_path.isEmpty() && remember_last_path ? current_file_path : basepath), browse_extension_filter);
    else
        file_name = QFileDialog::getSaveFileName(this, browse_caption, (!current_file_path.isEmpty() && remember_last_path ? current_file_path : basepath), browse_extension_filter);

    if(select_file(file_name, file_open_save_mode))
        emit selected_file(file_name);
}

// AUX
void QFileSelectionWidget::refresh_text(){
    QString file_name = current_file_path;
    if(!show_path)
        file_name = path_to_name(current_file_path);
    ui->lineEdit->setText(file_name);
}

// CONF
//show_path_mode
void QFileSelectionWidget::conf_flip_show_path_mode(){
    if(show_path && !ui->lineEdit->text().isEmpty())
        ui->lineEdit->setText(path_to_name(ui->lineEdit->text()));
    show_path = !show_path;
    refresh_text();
}
void QFileSelectionWidget::conf_set_show_path_mode(bool new_show_path){
    if(show_path != new_show_path)
        conf_flip_show_path_mode();
}
bool QFileSelectionWidget::conf_get_show_path_mode(){
    return show_path;
}
//editable_path
void QFileSelectionWidget::conf_flip_editable_path_state(){
    conf_set_editable_path_state(!conf_get_editable_path_state());
}
void QFileSelectionWidget::conf_set_editable_path_state(bool new_state){
    ui->lineEdit->setEnabled(new_state);
}
bool QFileSelectionWidget::conf_get_editable_path_state(){
    return ui->lineEdit->isEnabled();
}
//file_open_save_mode
void QFileSelectionWidget::conf_flip_file_open_save_mode(){
    file_open_save_mode = !file_open_save_mode;
}
void QFileSelectionWidget::conf_set_file_open_save_mode(bool new_file_mode){
    if(file_open_save_mode != new_file_mode)
        conf_flip_file_open_save_mode();
}
bool QFileSelectionWidget::conf_get_file_open_save_mode(){
    return file_open_save_mode;
}
//remember_last_path
void QFileSelectionWidget::conf_flip_remember_last_path(){
    remember_last_path = !remember_last_path;
}
void QFileSelectionWidget::conf_set_remember_last_path(bool new_remember){
    if(remember_last_path != new_remember)
        conf_flip_remember_last_path();
}
bool QFileSelectionWidget::conf_get_remember_last_path(){
    return remember_last_path;
}
//base_path
void QFileSelectionWidget::conf_set_basepath(QString new_basepath){
    basepath = (new_basepath.isEmpty()?QDir::currentPath():new_basepath);
}
QString QFileSelectionWidget::conf_get_basepath(){
    return basepath;
}
//browse_extension_filter
void QFileSelectionWidget::conf_set_browse_extension_filter(QString new_browse_extension_filter){
    browse_extension_filter = (new_browse_extension_filter.isEmpty()?"All Files (*.*)":new_browse_extension_filter);
}
QString QFileSelectionWidget::conf_get_browse_extension_filter(){
    return browse_extension_filter;
}
//accepts_drops
void QFileSelectionWidget::conf_flip_accepts_drops(){
    setAcceptDrops(!conf_get_accepts_drops());
}
void QFileSelectionWidget::conf_set_accepts_drops(bool new_accepts_drops){
    if(acceptDrops() != new_accepts_drops)
        conf_flip_accepts_drops();
}
bool QFileSelectionWidget::conf_get_accepts_drops(){
    return acceptDrops();
}

