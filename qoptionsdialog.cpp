#include "qoptionsdialog.h"
#include "ui_qoptionsdialog.h"

QOptionsDialog::QOptionsDialog(QWidget *parent, const QString &opts_path) :
    QDialog(parent),
    ui(new Ui::QOptionsDialog)
{
    ui->setupUi(this);
    //setWindowFlag(Qt::FramelessWindowHint, true);

    connect(ui->button_close, SIGNAL(clicked()), this, SLOT(slot_button_close()));
    connect(ui->button_save, SIGNAL(clicked()), this, SLOT(slot_button_save()));

    this->opts_path = opts_path;
    init_opts();
}
QOptionsDialog::~QOptionsDialog()
{
    delete ui;
}

void QOptionsDialog::init_opts(){
    labels_names_list = new QList<QLabel*>();
    lineedit_values_list = new QList<QLineEdit*>();
    layouts_opts_list = new QList<QHBoxLayout*>();

    QMap<QString, QString> opts = NorLib::options_get(opts_path);

    if(!opts.isEmpty()){
        auto keyz = opts.keys();
        auto valuez = opts.values();
        auto layout_vert = new QVBoxLayout();

        for(int i = 0; i < valuez.count(); i++){
            layouts_opts_list->append(new QHBoxLayout(this));

            labels_names_list->append(new QLabel(keyz.at(i), this));

            lineedit_values_list->append(new QLineEdit(valuez.at(i), this));

            layouts_opts_list->last()->addWidget(labels_names_list->last());
            layouts_opts_list->last()->addWidget(lineedit_values_list->last());

            layout_vert->addLayout(layouts_opts_list->last());
        }
        ui->groupBox->setLayout(layout_vert);
    } else
        ui->button_save->setEnabled(false);
}

void QOptionsDialog::slot_button_close(){
    close();
}
void QOptionsDialog::slot_button_save(){
    QMap<QString, QString> *opts = new QMap<QString, QString>();

    for(int i = 0; i < labels_names_list->count(); i++){
        opts->insert(labels_names_list->at(i)->text(), lineedit_values_list->at(i)->text());
    }

    if(!NorLib::options_set(opts_path, opts))
        QMessageBox::information(this, "Error saving", "Error while saving the file. Either the file is not writable, or there were no options to save.", QMessageBox::Ok);
}
