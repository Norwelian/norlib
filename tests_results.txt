********* Start testing of NorLib_tests *********
Config: Using QtTest library 5.11.2, Qt 5.11.2 (x86_64-little_endian-llp64 static release build; by GCC 8.2.0)
INFO   : NorLib_tests::initTestCase() entering
PASS   : NorLib_tests::initTestCase()
INFO   : NorLib_tests::print_QStringList() entering
QINFO  : NorLib_tests::print_QStringList(size_1) [V]
PASS   : NorLib_tests::print_QStringList(size_1)
QINFO  : NorLib_tests::print_QStringList(size_2) [V]
PASS   : NorLib_tests::print_QStringList(size_2)
QINFO  : NorLib_tests::print_QStringList(size_3) [V]
PASS   : NorLib_tests::print_QStringList(size_3)
QINFO  : NorLib_tests::print_QStringList(size_0) [I]
PASS   : NorLib_tests::print_QStringList(size_0)
INFO   : NorLib_tests::print_QMap() entering
QINFO  : NorLib_tests::print_QMap(size_2x2) [V]
PASS   : NorLib_tests::print_QMap(size_2x2)
QINFO  : NorLib_tests::print_QMap(size_3x3) [V]
PASS   : NorLib_tests::print_QMap(size_3x3)
QINFO  : NorLib_tests::print_QMap(size_0x0) [I]
PASS   : NorLib_tests::print_QMap(size_0x0)
INFO   : NorLib_tests::hash_file_check() entering
QINFO  : NorLib_tests::hash_file_check(wtf.png/SHA3_512) [V]
PASS   : NorLib_tests::hash_file_check(wtf.png/SHA3_512)
QINFO  : NorLib_tests::hash_file_check(wtf.png/SHA256) [V]
PASS   : NorLib_tests::hash_file_check(wtf.png/SHA256)
QINFO  : NorLib_tests::hash_file_check(wtf.png/MD5) [V]
PASS   : NorLib_tests::hash_file_check(wtf.png/MD5)
QINFO  : NorLib_tests::hash_file_check(music.mp3/SHA3_512) [V]
PASS   : NorLib_tests::hash_file_check(music.mp3/SHA3_512)
QINFO  : NorLib_tests::hash_file_check(music.mp3/SHA256) [V]
PASS   : NorLib_tests::hash_file_check(music.mp3/SHA256)
QINFO  : NorLib_tests::hash_file_check(music.mp3/MD5) [V]
PASS   : NorLib_tests::hash_file_check(music.mp3/MD5)
QINFO  : NorLib_tests::hash_file_check(no_file/SHA3_512) [I]
PASS   : NorLib_tests::hash_file_check(no_file/SHA3_512)
INFO   : NorLib_tests::format_number() entering
QINFO  : NorLib_tests::format_number(123.012) [V]
PASS   : NorLib_tests::format_number(123.012)
QINFO  : NorLib_tests::format_number(1823.2) [V]
PASS   : NorLib_tests::format_number(1823.2)
QINFO  : NorLib_tests::format_number(19782987.0723) [V]
PASS   : NorLib_tests::format_number(19782987.0723)
QINFO  : NorLib_tests::format_number(3.) [V]
PASS   : NorLib_tests::format_number(3.)
QINFO  : NorLib_tests::format_number(3) [V]
PASS   : NorLib_tests::format_number(3)
QINFO  : NorLib_tests::format_number(16152.) [V]
PASS   : NorLib_tests::format_number(16152.)
QINFO  : NorLib_tests::format_number(16152) [V]
PASS   : NorLib_tests::format_number(16152)
QINFO  : NorLib_tests::format_number(-123.012) [V]
PASS   : NorLib_tests::format_number(-123.012)
QINFO  : NorLib_tests::format_number(-1823.2) [V]
PASS   : NorLib_tests::format_number(-1823.2)
QINFO  : NorLib_tests::format_number(-19782987.0723) [V]
PASS   : NorLib_tests::format_number(-19782987.0723)
QINFO  : NorLib_tests::format_number(-3.) [V]
PASS   : NorLib_tests::format_number(-3.)
QINFO  : NorLib_tests::format_number(-3) [V]
PASS   : NorLib_tests::format_number(-3)
QINFO  : NorLib_tests::format_number(-16152.) [V]
PASS   : NorLib_tests::format_number(-16152.)
QINFO  : NorLib_tests::format_number(-16152) [V]
PASS   : NorLib_tests::format_number(-16152)
QINFO  : NorLib_tests::format_number('.') [I]
PASS   : NorLib_tests::format_number('.')
QINFO  : NorLib_tests::format_number('') [I]
PASS   : NorLib_tests::format_number('')
QINFO  : NorLib_tests::format_number('aa') [I]
PASS   : NorLib_tests::format_number('aa')
QINFO  : NorLib_tests::format_number('a.12312') [I]
PASS   : NorLib_tests::format_number('a.12312')
QINFO  : NorLib_tests::format_number('12312.a') [I]
PASS   : NorLib_tests::format_number('12312.a')
QINFO  : NorLib_tests::format_number('123a12.1211') [I]
PASS   : NorLib_tests::format_number('123a12.1211')
QINFO  : NorLib_tests::format_number('12312.121a1') [I]
PASS   : NorLib_tests::format_number('12312.121a1')
INFO   : NorLib_tests::get_set_option() entering
QINFO  : NorLib_tests::get_set_option(opt1 = 5) [V]
PASS   : NorLib_tests::get_set_option(opt1 = 5)
QINFO  : NorLib_tests::get_set_option(opt2 = hola) [V]
PASS   : NorLib_tests::get_set_option(opt2 = hola)
QINFO  : NorLib_tests::get_set_option(opt3 = ./shoit) [V]
PASS   : NorLib_tests::get_set_option(opt3 = ./shoit)
QINFO  : NorLib_tests::get_set_option(opt4 = asdasd {}[]?!-_.,:;^$%&@') [V]
PASS   : NorLib_tests::get_set_option(opt4 = asdasd {}[]?!-_.,:;^$%&@')
QINFO  : NorLib_tests::get_set_option(opt 123 = hola mundo!) [V]
PASS   : NorLib_tests::get_set_option(opt 123 = hola mundo!)
QINFO  : NorLib_tests::get_set_option(path = E:\test\file.whatever) [V]
PASS   : NorLib_tests::get_set_option(path = E:\test\file.whatever)
INFO   : NorLib_tests::get_set_options() entering
QINFO  : NorLib_tests::get_set_options(same_options_as_before_bulk) [V]
PASS   : NorLib_tests::get_set_options(same_options_as_before_bulk)
INFO   : NorLib_tests::cleanupTestCase() entering
PASS   : NorLib_tests::cleanupTestCase()
Totals: 44 passed, 0 failed, 0 skipped, 0 blacklisted, 254ms
********* Finished testing of NorLib_tests *********
