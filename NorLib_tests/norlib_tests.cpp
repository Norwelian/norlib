#include "norlib_tests.h"

QTEST_APPLESS_MAIN(NorLib_tests)

typedef QMap<QString, QStringList> QMap_type;
Q_DECLARE_METATYPE(QMap_type)

NorLib_tests::NorLib_tests(QObject *parent) : QObject(parent)
{
    valid = "[V]";
    invalid = "[I]";
}

void NorLib_tests::print_QStringList(){
    QFETCH(bool, validness);
    QFETCH(QStringList, data);
    QFETCH(QString, result);

    if(validness)
        qInfo(valid);
    else
        qInfo(invalid);

    QCOMPARE(NorLib::print_QStringList(&data), result);
    QCOMPARE(NorLib::print_QStringList(data), result);
}
void NorLib_tests::print_QStringList_data(){
    QTest::addColumn<bool>("validness");
    QTest::addColumn<QStringList>("data");
    QTest::addColumn<QString>("result");

    QStringList test_qsl1;
    test_qsl1.append("Hola");
    test_qsl1.append("Mundo");
    QStringList test_qsl2;
    test_qsl2.append("Prueba");

    QStringList test_qsl3;
    test_qsl3.append("ASdasdas");
    test_qsl3.append("2109310293");
    test_qsl3.append(QString::number(21093.2, 'f', 2));

    QStringList test_qsl4;

    QTest::newRow("size_1") << true << test_qsl2 << "[ 'Prueba' ]";
    QTest::newRow("size_2") << true << test_qsl1 << "[ 'Hola' 'Mundo' ]";
    QTest::newRow("size_3") << true << test_qsl3 << "[ 'ASdasdas' '2109310293' '21093.20' ]";
    QTest::newRow("size_0") << false << test_qsl4 << QString();
}

void NorLib_tests::print_QMap(){
    QFETCH(bool, validness);
    QFETCH(QMap_type, data1);
    QFETCH(QString, result);

    if(validness)
        qInfo(valid);
    else
        qInfo(invalid);

    QCOMPARE(NorLib::print_QMap(data1), result);
    QCOMPARE(NorLib::print_QMap(&data1), result);
}
void NorLib_tests::print_QMap_data(){
    QTest::addColumn<bool>("validness");
    QTest::addColumn<QMap_type>("data1");
    QTest::addColumn<QString>("result");

    QMap_type test1_m1;
    test1_m1.insert("key 0", QStringList() << "cosa 0 key 0" << "cosa 1 key 0" << "cosa 2 key 0");
    test1_m1.insert("key 1", QStringList() << "cosa 0 key 1" << "cosa 1 key 1" << "cosa 2 key 1");
    test1_m1.insert("key 2", QStringList() << "cosa 0 key 2" << "cosa 1 key 2" << "cosa 2 key 2");

    QMap_type test1_m3;
    test1_m3.insert("key 0", QStringList() << "cosa 0 key 0" << "cosa 1 key 0");
    test1_m3.insert("key 1", QStringList() << "cosa 0 key 1" << "cosa 1 key 1");

    QString aux_output_2 = "[ 'key 0' ][ 'cosa 0 key 0' 'cosa 1 key 0' ]\n[ 'key 1' ][ 'cosa 0 key 1' 'cosa 1 key 1' ]";
    QString aux_output_3 = "[ 'key 0' ][ 'cosa 0 key 0' 'cosa 1 key 0' 'cosa 2 key 0' ]\n[ 'key 1' ][ 'cosa 0 key 1' 'cosa 1 key 1' 'cosa 2 key 1' ]\n[ 'key 2' ][ 'cosa 0 key 2' 'cosa 1 key 2' 'cosa 2 key 2' ]";

    QMap_type test1_0_1;

    QTest::newRow("size_2x2") << true << test1_m3 << aux_output_2;
    QTest::newRow("size_3x3") << true << test1_m1 << aux_output_3;
    QTest::newRow("size_0x0") << false << test1_0_1 << QString();
}

void NorLib_tests::hash_file_check(){
    QFETCH(QCryptographicHash::Algorithm, data_algo);
    QFETCH(QString, data_file_path);
    QFETCH(bool, result);

    if(result)
        qInfo(valid);
    else
        qInfo(invalid);

    QByteArray test_hash_file = NorLib::file_hash_generate(data_algo, data_file_path);

    QCOMPARE(NorLib::file_hash_dump(data_algo, data_file_path), result);

    QCOMPARE(NorLib::file_hash_check(data_algo, data_file_path, test_hash_file), result);
    QCOMPARE(NorLib::file_hash_check(data_algo, data_file_path), result);
}
void NorLib_tests::hash_file_check_data(){
    QTest::addColumn<QCryptographicHash::Algorithm>("data_algo");
    QTest::addColumn<QString>("data_file_path");
    QTest::addColumn<bool>("result");

    QTest::newRow("wtf.png/SHA3_512") << QCryptographicHash::Algorithm::Sha3_512 << "test_files/wtf.png" << true;
    QTest::newRow("wtf.png/SHA256") << QCryptographicHash::Algorithm::Sha256 << "test_files/wtf.png" << true;
    QTest::newRow("wtf.png/MD5") << QCryptographicHash::Algorithm::Md5 << "test_files/wtf.png" << true;
    QTest::newRow("music.mp3/SHA3_512") << QCryptographicHash::Algorithm::Md5 << "test_files/music.mp3" << true;
    QTest::newRow("music.mp3/SHA256") << QCryptographicHash::Algorithm::Md5 << "test_files/music.mp3" << true;
    QTest::newRow("music.mp3/MD5") << QCryptographicHash::Algorithm::Md5 << "test_files/music.mp3" << true;
    QTest::newRow("no_file/SHA3_512") << QCryptographicHash::Algorithm::Sha3_512 << "no_file.bad" << false;
}

void NorLib_tests::format_number(){
    QFETCH(QString, data_str);
    QFETCH(bool, data_valid);
    if(data_valid){
        qInfo(valid);
        QFETCH(double, data_double);
        QFETCH(QString, result);

        QCOMPARE(NorLib::format_number(data_str), result);
        QCOMPARE(NorLib::format_number(&data_str), result);
        QCOMPARE(NorLib::format_number(data_double), result);
    } else {
        qInfo(invalid);
        QCOMPARE(NorLib::format_number(data_str), QString());
        QCOMPARE(NorLib::format_number(&data_str), QString());
    }
}
void NorLib_tests::format_number_data(){
    QTest::addColumn<QString>("data_str");
    QTest::addColumn<bool>("data_valid");
    QTest::addColumn<double>("data_double");
    QTest::addColumn<QString>("result");

    //POSITIVE
    QTest::newRow("123.012") << "123.012" << true << 123.012 << "123.01";
    QTest::newRow("1823.2") << "1823.2" << true << 1823.2 << "1,823.20";
    QTest::newRow("19782987.0723") << "19782987.0723" << true << 19782987.0723 << "19,782,987.07";
    QTest::newRow("3.") << "3." << true << 3. << "3.00";
    QTest::newRow("3") << "3" << true << static_cast<double>(3) << "3.00";
    QTest::newRow("16152.") << "16152." << true << 16152. << "16,152.00";
    QTest::newRow("16152") << "16152" << true << static_cast<double>(16152) << "16,152.00";
    //NEGATIVE
    QTest::newRow("-123.012") << "-123.012" << true << -123.012 << "-123.01";
    QTest::newRow("-1823.2") << "-1823.2" << true << -1823.2 << "-1,823.20";
    QTest::newRow("-19782987.0723") << "-19782987.0723" << true << -19782987.0723 << "-19,782,987.07";
    QTest::newRow("-3.") << "-3." << true << -3. << "-3.00";
    QTest::newRow("-3") << "-3" << true << static_cast<double>(-3) << "-3.00";
    QTest::newRow("-16152.") << "-16152." << true << -16152. << "-16,152.00";
    QTest::newRow("-16152") << "-16152" << true << static_cast<double>(-16152) << "-16,152.00";
    //INVALID
    QTest::newRow("'.'") << "." << false;
    QTest::newRow("''") << "" << false;
    QTest::newRow("'aa'") << "aa" << false;
    QTest::newRow("'a.12312'") << "a.12312" << false;
    QTest::newRow("'12312.a'") << "12312.a" << false;
    QTest::newRow("'123a12.1211'") << "123a12.1211" << false;
    QTest::newRow("'12312.121a1'") << "12312.121a1" << false;
}

void NorLib_tests::get_set_option(){
    QFETCH(bool, data_valid);
    QFETCH(QString, data_option_name);
    if(data_valid){
        qInfo(valid);
        QFETCH(QString, data_option_value);
        NorLib::option_set("test_files/opt_file_test.opts", data_option_name, data_option_value, true);
        QCOMPARE(NorLib::option_get("test_files/opt_file_test.opts", data_option_name), data_option_value);
    } else {
        QCOMPARE(NorLib::option_get("test_files/opt_file_test.opts", data_option_name), QString());
    }
}
void NorLib_tests::get_set_option_data(){
    QTest::addColumn<bool>("data_valid");
    QTest::addColumn<QString>("data_option_name");
    QTest::addColumn<QString>("data_option_value");

    //Valid tests
    QTest::newRow("opt1 = 5") << true << "opt1" << "5";
    QTest::newRow("opt2 = hola") << true << "opt2" << "hola";
    QTest::newRow("opt3 = ./shoit") << true << "opt3" << "./shoit";
    QTest::newRow("opt4 = asdasd {}[]?!-_.,:;^$%&@'") << true << "opt4" << "asdasd {}[]?!-_.,:;^$%&@'";
    QTest::newRow("opt 123 = hola mundo!") << true << "opt 123" << "hola mundo!";
    QTest::newRow("path = E:\\test\\file.whatever") << true << "path" << "E:\\test\\file.whatever";
}

void NorLib_tests::get_set_options(){
    QFETCH(QStringList, data_options_name);
    QFETCH(QStringList, data_options_value);
    auto options_and_values = new QMap<QString, QString>();
    for(int i = 0; i < data_options_name.count(); i++)
        options_and_values->insert(data_options_name.at(i), data_options_value.at(i));

    qInfo(valid);

    QCOMPARE(NorLib::options_set("test_files/opt_file_test_bulk.opts", options_and_values), true);
    QCOMPARE(NorLib::options_get("test_files/opt_file_test_bulk.opts"), *options_and_values);
    for(int i = 0; i < data_options_name.count(); i++)
        QCOMPARE(NorLib::option_get("test_files/opt_file_test_bulk.opts", data_options_name.at(i)), data_options_value.at(i));
}
void NorLib_tests::get_set_options_data(){
    QTest::addColumn<QStringList>("data_options_name");
    QTest::addColumn<QStringList>("data_options_value");

    QTest::newRow("same_options_as_before_bulk") <<
        (QStringList() << "opt1" << "opt2" << "opt3" << "opt4" << "opt 123" << "path") <<
        (QStringList() << "5" << "hola" << "./shoit" << "asdasd {}[]?!-_.,:;^$%&@'" << "hola mundo!" << "E:\\test\\file.whatever");
}

