#ifndef NORLIB_TESTS_H
#define NORLIB_TESTS_H

#include <QObject>
#include <QtTest/QtTest>
#include <norlib.h>

class NorLib_tests : public QObject
{
    Q_OBJECT
public:
    explicit NorLib_tests(QObject *parent = nullptr);

signals:

private:
    const char *valid;
    const char *invalid;
private slots:
    void print_QStringList();
    void print_QStringList_data();
    void print_QMap();
    void print_QMap_data();
    void hash_file_check();
    void hash_file_check_data();
    void format_number();
    void format_number_data();
    void get_set_option();
    void get_set_option_data();
    void get_set_options();
    void get_set_options_data();
};

#endif // NORLIB_TESTS_H
