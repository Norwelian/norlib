#include "qprocessqueue.h"

QProcessQueue::QProcessQueue()
{
    proc = new QProcess(this);
    ext_queue = new QQueue<QStringList>();
    timer = new QTimer(this);

    connect(proc, SIGNAL(finished(int)), this, SLOT(end_proc(int)));
    connect(proc, SIGNAL(readyReadStandardOutput()), this, SLOT(proc_read()));
}
QProcessQueue::~QProcessQueue()
{
    if(process_started())
        proc->kill();
    proc->deleteLater();
    delete ext_queue;
    if(timer->isActive())
        timer->stop();
    timer->deleteLater();
}

void QProcessQueue::start(){
    if(!process_started()){
        proc_output = "";
        if (!timer->isActive())
            timer->singleShot(1000, this, SLOT(queue_tick()));
    }
}
void QProcessQueue::stop(){
    if(process_started()){
        proc->terminate();
        proc->deleteLater();
        if (timer->isActive())
            timer->stop();
        proc = new QProcess(this);
        connect(proc, SIGNAL(finished(int)), this, SLOT(end_proc(int)));
        connect(proc, SIGNAL(readyReadStandardOutput()), this, SLOT(proc_read()));
    }
}

void QProcessQueue::queue_add(const QStringList& argz){
    ext_queue->enqueue(argz);
}

void QProcessQueue::queue_add_start(const QStringList& argz){
    queue_add(argz);
    start();
}

void QProcessQueue::set_shell_command(QString new_cmd){
    shell_command = (new_cmd.isEmpty()?"cmd.exe":new_cmd);
}

void QProcessQueue::queue_tick(){
    if(!process_started()){
        if(!queue_isEmpty()){
            auto argz = ext_queue->dequeue();
//            for(int i = 0; i < argz.count(); i++){
//                auto aux_str = argz.at(i);
//                if(aux_str.contains("/"))
//                    argz.replace(i, aux_str.replace("/", "\\"));
//            }
            argz.insert(0, "/C"); // for cmd.exe, flag that executes the command and exits the shell
            proc->start(shell_command, argz);
            timer->singleShot(1000, this, SLOT(queue_tick()));
        } else {
            emit all_proc_end();
        }
    } else
        timer->singleShot(1000, this, SLOT(queue_tick()));
}

void QProcessQueue::proc_read(){
    proc_output.append(proc->readAllStandardOutput());
}

void QProcessQueue::end_proc(int e){
    proc->deleteLater();
    proc = new QProcess(this);
    connect(proc, SIGNAL(finished(int)), this, SLOT(end_proc(int)));
    connect(proc, SIGNAL(readyReadStandardOutput()), this, SLOT(proc_read()));
    emit current_proc_end();
}

bool QProcessQueue::process_started(){
    return proc->processId() != 0;
}
bool QProcessQueue::queue_isEmpty(){
    return ext_queue->isEmpty();
}
int QProcessQueue::queue_count(){
    return ext_queue->count();
}
QString QProcessQueue::process_output(){
    return proc_output;
}
